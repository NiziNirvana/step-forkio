const logoBlock = document.querySelector(".header-logo");
const logoImg = document.querySelector(".logo-img .logo-img-item");
const logoText =document.querySelector(".logo-text");

logoBlock.addEventListener("mouseover", function (){
    logoImg.src = "./dist/img/header/logo-hover.png";
    logoText.style.color = "#fff";
    event.target.style.cursor = "pointer";
})

logoBlock.addEventListener("mouseout", function (){
    logoImg.src = "./dist/img/header/logo_company.png";
    logoText.style.color = "#8D81AC";
    event.target.style.cursor = "pointer";
})